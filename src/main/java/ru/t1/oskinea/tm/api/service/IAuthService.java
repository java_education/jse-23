package ru.t1.oskinea.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.oskinea.tm.enumerated.Role;
import ru.t1.oskinea.tm.model.User;

public interface IAuthService {

    void checkRoles(@Nullable Role[] roles);

    @NotNull
    User getUser();

    @NotNull
    String getUserId();

    boolean isAuth();

    void login(@NotNull String login, @NotNull String password);

    void logout();

    @NotNull
    User registry(@NotNull String login, @NotNull String password, @NotNull String email);

}
