package ru.t1.oskinea.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.oskinea.tm.enumerated.Role;
import ru.t1.oskinea.tm.model.User;

public interface IUserService extends IService<User> {

    @NotNull
    User create(@NotNull String login, @NotNull String password);

    @NotNull
    User create(@NotNull String login, @NotNull String password, @NotNull String email);

    @NotNull
    User create(@NotNull String login, @NotNull String password, @Nullable Role role);

    @Nullable
    User findByEmail(@NotNull String email);

    @Nullable
    User findByLogin(@NotNull String login);

    boolean isEmailExist(@NotNull String email);

    boolean isLoginExist(@NotNull String login);

    void lockUserByLogin(@NotNull String login);

    @Nullable
    @SuppressWarnings("unused")
    User removeByEmail(@NotNull String email);

    @Nullable
    @SuppressWarnings("UnusedReturnValue")
    User removeByLogin(@NotNull String login);

    @NotNull
    @SuppressWarnings("UnusedReturnValue")
    User setPassword(@NotNull String id, @NotNull String password);

    void unlockUserByLogin(@NotNull String login);

    @NotNull
    @SuppressWarnings("UnusedReturnValue")
    User updateUser(
            @NotNull String id,
            @NotNull String firstName,
            @NotNull String lastName,
            @NotNull String middleName
    );

}
