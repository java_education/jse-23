package ru.t1.oskinea.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.oskinea.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public interface IRepository<M extends AbstractModel> {

    interface IRepositoryOptional<M> {

        @NotNull
        Optional<M> findOneById(@NotNull String id);

        @NotNull
        Optional<M> findOneByIndex(@NotNull Integer index);

    }

    @SuppressWarnings("unused")
    default IRepositoryOptional<M> optional() {
        return new IRepositoryOptional<M>() {
            @NotNull
            @Override
            public Optional<M> findOneById(@NotNull final String id) {
                return Optional.ofNullable(IRepository.this.findOneById(id));
            }

            @NotNull
            @Override
            public Optional<M> findOneByIndex(@NotNull final Integer index) {
                return Optional.ofNullable(IRepository.this.findOneByIndex(index));
            }
        };
    }

    @NotNull
    M add(@NotNull M model);

    void clear();

    boolean existsById(@NotNull String id);

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(@Nullable Comparator<M> comparator);

    @Nullable
    M findOneById(@NotNull String id);

    @Nullable
    M findOneByIndex(@NotNull Integer index);

    int getSize();

    @Nullable
    M remove(@Nullable M model);

    void removeAll(@Nullable Collection<M> collection);

    @Nullable
    M removeById(@NotNull String id);

    @Nullable
    M removeByIndex(@NotNull Integer index);

}
